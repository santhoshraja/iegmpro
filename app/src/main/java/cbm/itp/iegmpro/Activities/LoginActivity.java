package cbm.itp.iegmpro.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ara.sandy.candies.dialog.ProgressDialog;
import ara.sandy.candies.utils.Utility;
import ara.sandy.candies.volley.GSONDateRequest;
import ara.sandy.candies.volley.GSONRequest;
import ara.sandy.candies.volley.GSONjsonRequest;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cbm.itp.iegmpro.Modal.Result;
import cbm.itp.iegmpro.R;
import cbm.itp.iegmpro.utils.Constant;
import cbm.itp.iegmpro.utils.PreferenceUtils;

import static cbm.itp.iegmpro.utils.Constant.DB_STRING;

public class LoginActivity extends AppCompatActivity {
    Activity mContext = this;

    @BindView(R.id.btnSignIn)
    Button btnSignIn;

    @BindView(R.id.txtUsername)
    EditText txtUsername;

    @BindView(R.id.txtPassword)
    EditText txtPassword;

    @BindView(R.id.lblForgot)
    TextView lblForgot;

    @BindView(R.id.chkRemember)
    CheckBox chkRemember;

    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        ButterKnife.bind(mContext);

        if (PreferenceUtils.getRemember(getApplicationContext()) == 1){
            startActivity(new Intent(mContext,DashboardActivity.class));
        }

        txtPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP)
                {
                    if (txtPassword.getCompoundDrawables()[2] != null)
                    {
                        if (event.getX() >= (txtPassword.getRight() - txtPassword.getLeft() - txtPassword.getCompoundDrawables()[2].getBounds().width()))
                        {
                            if(txtPassword.getTransformationMethod() == PasswordTransformationMethod.getInstance()){
                                txtPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                            }else{
                                txtPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                            }
                        }
                    }
                }
                return false;
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            finishAffinity();
        }
    }

    @OnClick(R.id.btnSignIn)
    void signIn(){
        dialog = new ProgressDialog(this);
        dialog.setMessage("Authenticating...");
        dialog.setCancelable(false);
        dialog.show();

        Map<String,String> params = new HashMap<>();
        params.put("UserId",txtUsername.getText().toString());
        params.put("UserPass",txtPassword.getText().toString());
        params.put("db_string",DB_STRING);

        String url = Constant.BASE_URL + Constant.LOGIN;
        RequestQueue mRequestQueue = Volley.newRequestQueue(this);

        Map<String,String> header = new HashMap<String,String>();
        header.put("Accept","application/json");

        GSONDateRequest gsoNjsonRequest = new GSONDateRequest
                (Request.Method.POST, url,String.class,
                        params,
                        submitSuccessListener(),
                        submitErrorListener());
        gsoNjsonRequest.setHeader(header);
        mRequestQueue.add(gsoNjsonRequest);

    }

    private Response.Listener submitSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String results) {

                if (dialog != null){
                    dialog.dismiss();
                }

                Gson gson = new Gson();
                List<Result> resultList = new ArrayList<Result>(Arrays.asList(gson.fromJson(results, Result[].class)));
                Result response = resultList.get(0);

                if (response.getUserCode() != "-1") {

                    if (TextUtils.equals(response.getLicenseType(),"3")){
                        startActivity(new Intent(LoginActivity.this,DashboardActivity.class));
                    } else {
                        Utility.showMessage(mContext,"Invalid License Type");
                        return;
                    }

                    int remember = 0;
                    if (chkRemember.isChecked()){
                     remember = 1;
                    }

                    PreferenceUtils.setUserDetails(getApplicationContext(),
                            response.getUserCode(),
                            response.getPortCode(),
                            response.getUserId(),
                            response.getLicenseType(),
                            response.getCompanyCode(),
                            response.getCompanyId(),
                            response.getCompanyName(),
                            response.getLogKey(),
                            response.getLogInTime(),
                            response.getConnString(),
                            remember);
                } else {
                    Utility.showMessage(mContext,response.getUserId());
                }
            }
        };
    }

    private Response.ErrorListener submitErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (dialog != null){
                    dialog.dismiss();
                }
                error.printStackTrace();
                Utility.showMessage(mContext,error.getMessage());
            }
        };
    }

}
