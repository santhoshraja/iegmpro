package cbm.itp.iegmpro.Activities;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ara.sandy.candies.dialog.ProgressDialog;
import ara.sandy.candies.utils.SeparatorDecoration;
import ara.sandy.candies.utils.Utility;
import ara.sandy.candies.volley.GSONDateRequest;
import butterknife.BindView;
import butterknife.ButterKnife;
import cbm.itp.iegmpro.Adapter.ContainerAdapter;
import cbm.itp.iegmpro.Adapter.RotationCHAAdapter;
import cbm.itp.iegmpro.Adapter.SBAdapter;
import cbm.itp.iegmpro.Modal.CHA;
import cbm.itp.iegmpro.Modal.Container;
import cbm.itp.iegmpro.Modal.SB;
import cbm.itp.iegmpro.R;
import cbm.itp.iegmpro.utils.Constant;
import cbm.itp.iegmpro.utils.PreferenceUtils;

public class SBActivity extends AppCompatActivity {
    @BindView(R.id.txtCHAName)
    TextView txtCHAName;

    @BindView(R.id.lstMain)
    RecyclerView lstMain;

    @BindView(R.id.lstContainer)
    RecyclerView lstContainer;

    private Activity mContext = this;

    List<SB> sbList = new ArrayList<>();
    List<Container> contList = new ArrayList<>();
    SBAdapter adapter;
    ContainerAdapter containerAdapter;

    ProgressDialog dialog;

    String rotationCode;
    String cfCode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sb);
        ButterKnife.bind(mContext);

        Bundle args = getIntent().getExtras();
        rotationCode = args.getString("ROTATION_CODE");
        txtCHAName.setText(args.getString("CHANAME"));
        cfCode = args.getString("CFCODE");

        LinearLayoutManager layoutManager = new LinearLayoutManager(lstMain.getContext(),LinearLayoutManager.VERTICAL,false);
        lstMain.addItemDecoration(new SeparatorDecoration(mContext,getResources().getColor(R.color.subTextColor),0.5f));
        lstMain.setLayoutManager(layoutManager);
        lstMain.setHasFixedSize(false);


        adapter = new SBAdapter(sbList,mContext);
        lstMain.setAdapter(adapter);

        LinearLayoutManager contlayoutManager = new LinearLayoutManager(lstContainer.getContext(),LinearLayoutManager.VERTICAL,false);
        lstContainer.addItemDecoration(new SeparatorDecoration(mContext,getResources().getColor(R.color.subTextColor),0.5f));
        lstContainer.setLayoutManager(contlayoutManager);
        lstContainer.setHasFixedSize(false);

        containerAdapter = new ContainerAdapter(contList,mContext);
        lstContainer.setAdapter(containerAdapter);

        getSB();
        getContainer();
    }


    void getSB(){
        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        String strFilter = "";
        strFilter = " And CompanyCode =" + PreferenceUtils.getCompanyCode(getApplicationContext()) +
                " And RotationCode =" + rotationCode + " And CFCode = " + cfCode;

        Map<String,String> params = new HashMap<>();
        params.put("StrFilter",strFilter);
        params.put("db_string",PreferenceUtils.getConnString(getApplicationContext()));

        String url = Constant.BASE_URL + Constant.GET_EDITEGMSB;
        RequestQueue mRequestQueue = Volley.newRequestQueue(this);


        Map<String,String> header = new HashMap<String,String>();
        header.put("Accept","application/json");

        GSONDateRequest gsoNjsonRequest = new GSONDateRequest
                (Request.Method.POST, url,String.class,
                        params,
                        submitSuccessListener(),
                        submitErrorListener());
        gsoNjsonRequest.setHeader(header);
        mRequestQueue.add(gsoNjsonRequest);

    }

    private Response.Listener submitSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String results) {

                if (dialog != null){
                    dialog.dismiss();
                }

                Gson gson = new Gson();
                sbList = new ArrayList<SB>(Arrays.asList(gson.fromJson(results, SB[].class)));
                adapter.setSBs(sbList);
                adapter.notifyDataSetChanged();
            }
        };
    }

    private Response.ErrorListener submitErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (dialog != null){
                    dialog.dismiss();
                }
                error.printStackTrace();
                Utility.showMessage(mContext,error.getMessage());
            }
        };
    }


    void getContainer(){

        String strFilter = "";
        strFilter = " And CompanyCode =" + PreferenceUtils.getCompanyCode(getApplicationContext()) +
                " And RotationCode =" + rotationCode + " And CFCode = " + cfCode;

        Map<String,String> params = new HashMap<>();
        params.put("StrFilter",strFilter);
        params.put("db_string",PreferenceUtils.getConnString(getApplicationContext()));

        String url = Constant.BASE_URL + Constant.GET_EDITEGMCON;
        RequestQueue mRequestQueue = Volley.newRequestQueue(this);


        Map<String,String> header = new HashMap<String,String>();
        header.put("Accept","application/json");

        GSONDateRequest gsoNjsonRequest = new GSONDateRequest
                (Request.Method.POST, url,String.class,
                        params,
                        contSuccessListener(),
                        contErrorListener());
        gsoNjsonRequest.setHeader(header);
        mRequestQueue.add(gsoNjsonRequest);

    }

    private Response.Listener contSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String results) {

                if (dialog != null){
                    dialog.dismiss();
                }

                Gson gson = new Gson();
                contList = new ArrayList<Container>(Arrays.asList(gson.fromJson(results, Container[].class)));
                containerAdapter.setContainers(contList);
                containerAdapter.notifyDataSetChanged();
            }
        };
    }

    private Response.ErrorListener contErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (dialog != null){
                    dialog.dismiss();
                }
                error.printStackTrace();
                Utility.showMessage(mContext,error.getMessage());
            }
        };
    }


}
