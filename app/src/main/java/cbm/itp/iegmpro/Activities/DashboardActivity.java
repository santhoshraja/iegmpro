package cbm.itp.iegmpro.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceGroup;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ara.sandy.candies.dialog.ProgressDialog;
import ara.sandy.candies.utils.SeparatorDecoration;
import ara.sandy.candies.utils.Utility;
import ara.sandy.candies.volley.GSONDateRequest;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cbm.itp.iegmpro.Adapter.RotationAdapter;
import cbm.itp.iegmpro.Modal.Result;
import cbm.itp.iegmpro.Modal.Rotation;
import cbm.itp.iegmpro.R;
import cbm.itp.iegmpro.utils.Constant;
import cbm.itp.iegmpro.utils.PreferenceUtils;

import static cbm.itp.iegmpro.utils.Constant.DB_STRING;

public class DashboardActivity extends AppCompatActivity{

    @BindView(R.id.fab)
    FloatingActionButton fab;

    @BindView(R.id.lstMain)
    RecyclerView lstMain;

    @BindView(R.id.lblMessage)
    TextView lblMessage;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    @BindView(R.id.nav_view)
    NavigationView navigationView;

    private Activity mContext = this;

    List<Rotation> rotations = new ArrayList<>();
    RotationAdapter adapter;

    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.bind(mContext);
        setSupportActionBar(toolbar);
        setTitle(PreferenceUtils.getCompanyName(getApplicationContext()));
        toolbar.setSubtitle(PreferenceUtils.getCompanyId(getApplicationContext()));
        init();

        LinearLayoutManager layoutManager = new LinearLayoutManager(lstMain.getContext(),LinearLayoutManager.VERTICAL,false);
        lstMain.addItemDecoration(new SeparatorDecoration(mContext,getResources().getColor(R.color.subTextColor),0.5f));
        lstMain.setLayoutManager(layoutManager);
        lstMain.setHasFixedSize(false);

        adapter = new RotationAdapter(rotations,mContext);
        lstMain.setAdapter(adapter);

        getRotation();

    }

    private void init(){
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//        drawer.addDrawerListener(toggle);
//        toggle.syncState();
//
//        navigationView.setNavigationItemSelectedListener(this);
    }


    @OnClick(R.id.fab)
    void fabClick(View view){
        Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    @Override
    public void onBackPressed() {
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else {
            finishAffinity();
//        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dashboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {

            PreferenceUtils.clearUserDetails(getApplicationContext());
            startActivity(new Intent(mContext,LoginActivity.class));


            return true;
        }

        return super.onOptionsItemSelected(item);
    }

//    @SuppressWarnings("StatementWithEmptyBody")
//    @Override
//    public boolean onNavigationItemSelected(MenuItem item) {
//        // Handle navigation view item clicks here.
//        int id = item.getItemId();
//
//        if (id == R.id.nav_camera) {
//            // Handle the camera action
//        } else if (id == R.id.nav_gallery) {
//
//        } else if (id == R.id.nav_slideshow) {
//
//        } else if (id == R.id.nav_manage) {
//
//        } else if (id == R.id.nav_share) {
//
//        } else if (id == R.id.nav_send) {
//
//        }
//
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        drawer.closeDrawer(GravityCompat.START);
//        return true;
//    }

    void getRotation(){
        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        Map<String,String> params = new HashMap<>();
        params.put("StrFilter"," And CompanyCode = " + PreferenceUtils.getCompanyCode(getApplicationContext()));
        params.put("db_string",PreferenceUtils.getConnString(getApplicationContext()));

        String url = Constant.BASE_URL + Constant.GET_EGMDATA;
        RequestQueue mRequestQueue = Volley.newRequestQueue(this);

        Map<String,String> header = new HashMap<String,String>();
        header.put("Accept","application/json");

        GSONDateRequest gsoNjsonRequest = new GSONDateRequest
                (Request.Method.POST, url,String.class,
                        params,
                        submitSuccessListener(),
                        submitErrorListener());
        gsoNjsonRequest.setHeader(header);
        mRequestQueue.add(gsoNjsonRequest);

    }

    private Response.Listener submitSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String results) {

                if (dialog != null){
                    dialog.dismiss();
                }

                Gson gson = new Gson();
                rotations = new ArrayList<Rotation>(Arrays.asList(gson.fromJson(results, Rotation[].class)));
                adapter.setRotations(rotations);
                adapter.notifyDataSetChanged();

                if (rotations.isEmpty()){
                    lblMessage.setVisibility(View.VISIBLE);
                } else {
                    lblMessage.setVisibility(View.GONE);
                }
            }
        };
    }

    private Response.ErrorListener submitErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (dialog != null){
                    dialog.dismiss();
                }
                error.printStackTrace();
                Utility.showMessage(mContext,error.getMessage());
            }
        };
    }

}
