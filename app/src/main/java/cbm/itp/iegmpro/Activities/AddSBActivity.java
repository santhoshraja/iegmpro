package cbm.itp.iegmpro.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import cbm.itp.iegmpro.R;

public class AddSBActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_sb);
        getSupportActionBar().setElevation(1.5f);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_black);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("SB Details");

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
