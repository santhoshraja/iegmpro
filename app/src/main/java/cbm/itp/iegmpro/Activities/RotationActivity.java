package cbm.itp.iegmpro.Activities;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ara.sandy.candies.dialog.ProgressDialog;
import ara.sandy.candies.utils.SeparatorDecoration;
import ara.sandy.candies.utils.Utility;
import ara.sandy.candies.volley.GSONDateRequest;
import butterknife.BindView;
import butterknife.ButterKnife;
import cbm.itp.iegmpro.Adapter.RotationCHAAdapter;
import cbm.itp.iegmpro.Modal.CHA;
import cbm.itp.iegmpro.R;
import cbm.itp.iegmpro.utils.Constant;
import cbm.itp.iegmpro.utils.PreferenceUtils;

public class RotationActivity extends AppCompatActivity {

    @BindView(R.id.txtRNo)
    TextView txtRNo;

    @BindView(R.id.txtSB)
    TextView txtSB;

    @BindView(R.id.txtCont)
    TextView txtCont;

    @BindView(R.id.lstMain)
    RecyclerView lstMain;

    private Activity mContext = this;

    List<CHA> chaList = new ArrayList<>();
    RotationCHAAdapter adapter;

    ProgressDialog dialog;

    String rotationCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rotation);
        ButterKnife.bind(mContext);

        Bundle args = getIntent().getExtras();
        rotationCode = args.getString("ROTATION_CODE");
        txtRNo.setText(args.getString("ROTATION"));
        txtSB.setText(args.getString("SBCOUNT"));
        txtCont.setText(args.getString("CONTCOUNT"));

        LinearLayoutManager layoutManager = new LinearLayoutManager(lstMain.getContext(),LinearLayoutManager.VERTICAL,false);
        lstMain.addItemDecoration(new SeparatorDecoration(mContext,getResources().getColor(R.color.subTextColor),0.5f));
        lstMain.setLayoutManager(layoutManager);
        lstMain.setHasFixedSize(false);

        adapter = new RotationCHAAdapter(chaList,mContext);
        lstMain.setAdapter(adapter);

        getCHA();
    }



    void getCHA(){
        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();


        String strFilter = "";
        strFilter = " And CompanyCode =" + PreferenceUtils.getCompanyCode(getApplicationContext()) +
                " And RotationCode =" + rotationCode;

        Map<String,String> params = new HashMap<>();
        params.put("StrFilter",strFilter);
        params.put("db_string",PreferenceUtils.getConnString(getApplicationContext()));

        String url = Constant.BASE_URL + Constant.GET_EGMSBDATA;
        RequestQueue mRequestQueue = Volley.newRequestQueue(this);


        Map<String,String> header = new HashMap<String,String>();
        header.put("Accept","application/json");

        GSONDateRequest gsoNjsonRequest = new GSONDateRequest
                (Request.Method.POST, url,String.class,
                        params,
                        submitSuccessListener(),
                        submitErrorListener());
        gsoNjsonRequest.setHeader(header);
        mRequestQueue.add(gsoNjsonRequest);

    }

    private Response.Listener submitSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String results) {

                if (dialog != null){
                    dialog.dismiss();
                }

                Gson gson = new Gson();
                chaList = new ArrayList<CHA>(Arrays.asList(gson.fromJson(results, CHA[].class)));
                adapter.setCHAs(chaList);
                adapter.notifyDataSetChanged();
            }
        };
    }

    private Response.ErrorListener submitErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (dialog != null){
                    dialog.dismiss();
                }
                error.printStackTrace();
                Utility.showMessage(mContext,error.getMessage());
            }
        };
    }


}
