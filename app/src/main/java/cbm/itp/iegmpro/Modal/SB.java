package cbm.itp.iegmpro.Modal;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SB implements Serializable {

    @SerializedName("Key")
    String key;

    @SerializedName("SBNo")
    String sbNo;

    @SerializedName("SBDate")
    String sbDate;

    @SerializedName("NoOfPackages")
    String noOfPkgs;

    @SerializedName("POO")
    String orginPort;

    @SerializedName("POD")
    String destPort;

    @SerializedName("POG")
    String gatewayport;

    @SerializedName("dCargoDescr")
    String cargoDescr;

    @SerializedName("GrossWeight")
    String grossWeight;

    @SerializedName("contSize")
    String containerSize;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getSbNo() {
        return sbNo;
    }

    public void setSbNo(String sbNo) {
        this.sbNo = sbNo;
    }

    public String getSbDate() {
        return sbDate;
    }

    public void setSbDate(String sbDate) {
        this.sbDate = sbDate;
    }

    public String getNoOfPkgs() {
        return noOfPkgs;
    }

    public void setNoOfPkgs(String noOfPkgs) {
        this.noOfPkgs = noOfPkgs;
    }

    public String getOrginPort() {
        return orginPort;
    }

    public void setOrginPort(String orginPort) {
        this.orginPort = orginPort;
    }

    public String getDestPort() {
        return destPort;
    }

    public void setDestPort(String destPort) {
        this.destPort = destPort;
    }

    public String getGatewayport() {
        return gatewayport;
    }

    public void setGatewayport(String gatewayport) {
        this.gatewayport = gatewayport;
    }

    public String getCargoDescr() {
        return cargoDescr;
    }

    public void setCargoDescr(String cargoDescr) {
        this.cargoDescr = cargoDescr;
    }

    public String getGrossWeight() {
        return grossWeight;
    }

    public void setGrossWeight(String grossWeight) {
        this.grossWeight = grossWeight;
    }

    public String getContainerSize() {
        return containerSize;
    }

    public void setContainerSize(String containerSize) {
        this.containerSize = containerSize;
    }
}