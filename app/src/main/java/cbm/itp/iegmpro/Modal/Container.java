package cbm.itp.iegmpro.Modal;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Container implements Serializable {

    @SerializedName("AEKey")
    String key;

    @SerializedName("SBNo")
    String sbNo;

    @SerializedName("ContainerNo")
    String container;

    @SerializedName("ContainerSize")
    String contSize;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getSbNo() {
        return sbNo;
    }

    public void setSbNo(String sbNo) {
        this.sbNo = sbNo;
    }

    public String getContainer() {
        return container;
    }

    public void setContainer(String container) {
        this.container = container;
    }

    public String getContSize() {
        return contSize;
    }

    public void setContSize(String contSize) {
        this.contSize = contSize;
    }
}