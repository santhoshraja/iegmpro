package cbm.itp.iegmpro.Modal;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Rotation implements Serializable {

    @SerializedName("RotationCode")
    String key;

    @SerializedName("VesselId")
    String vesselId;

    @SerializedName("VesselName")
    String vesselName;

    @SerializedName("RotationNo")
    String rotationNo;

    @SerializedName("TargetDate")
    String rotationDate;

    @SerializedName("SailDate")
    String voyageDate;

    @SerializedName("VoyageNo")
    String voyageNo;

    @SerializedName("SBCount")
    String noOfSB;

    @SerializedName("ContCount")
    String noOfContainers;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getVesselId() {
        return vesselId;
    }

    public void setVesselId(String vesselId) {
        this.vesselId = vesselId;
    }

    public String getVesselName() {
        return vesselName;
    }

    public void setVesselName(String vesselName) {
        this.vesselName = vesselName;
    }

    public String getRotationNo() {
        return rotationNo;
    }

    public void setRotationNo(String rotationNo) {
        this.rotationNo = rotationNo;
    }

    public String getRotationDate() {
        return rotationDate;
    }

    public void setRotationDate(String rotationDate) {
        this.rotationDate = rotationDate;
    }

    public String getVoyageDate() {
        return voyageDate;
    }

    public void setVoyageDate(String voyageDate) {
        this.voyageDate = voyageDate;
    }

    public String getVoyageNo() {
        return voyageNo;
    }

    public void setVoyageNo(String voyageNo) {
        this.voyageNo = voyageNo;
    }

    public String getNoOfSB() {
        return noOfSB;
    }

    public void setNoOfSB(String noOfSB) {
        this.noOfSB = noOfSB;
    }

    public String getNoOfContainers() {
        return noOfContainers;
    }

    public void setNoOfContainers(String noOfContainers) {
        this.noOfContainers = noOfContainers;
    }
}
