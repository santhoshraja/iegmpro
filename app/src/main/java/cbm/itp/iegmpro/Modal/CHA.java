package cbm.itp.iegmpro.Modal;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class CHA implements Serializable {

    @SerializedName("CFCode")
    String cfCode;

    @SerializedName("RotationCode")
    String rotationCode;

    @SerializedName("RotationNo")
    String rotationNo;

    @SerializedName("CFName")
    String cfName;

    @SerializedName("CFConnString")
    String cfConnString;

    @SerializedName("CFWebServer")
    String cfWebServer;

    @SerializedName("CFCorresEmail")
    String cfCorresEmail;

    @SerializedName("ShippingBills")
    String shippingBills;

    @SerializedName("SBCount")
    String sbCount;

    @SerializedName("Containers")
    String containers;

    @SerializedName("ContCount")
    String contCount;

    public String getCfCode() {
        return cfCode;
    }

    public void setCfCode(String cfCode) {
        this.cfCode = cfCode;
    }

    public String getRotationCode() {
        return rotationCode;
    }

    public void setRotationCode(String rotationCode) {
        this.rotationCode = rotationCode;
    }

    public String getRotationNo() {
        return rotationNo;
    }

    public void setRotationNo(String rotationNo) {
        this.rotationNo = rotationNo;
    }

    public String getCfName() {
        return cfName;
    }

    public void setCfName(String cfName) {
        this.cfName = cfName;
    }

    public String getCfConnString() {
        return cfConnString;
    }

    public void setCfConnString(String cfConnString) {
        this.cfConnString = cfConnString;
    }

    public String getCfWebServer() {
        return cfWebServer;
    }

    public void setCfWebServer(String cfWebServer) {
        this.cfWebServer = cfWebServer;
    }

    public String getCfCorresEmail() {
        return cfCorresEmail;
    }

    public void setCfCorresEmail(String cfCorresEmail) {
        this.cfCorresEmail = cfCorresEmail;
    }

    public String getShippingBills() {
        return shippingBills;
    }

    public void setShippingBills(String shippingBills) {
        this.shippingBills = shippingBills;
    }

    public String getSbCount() {
        return sbCount;
    }

    public void setSbCount(String sbCount) {
        this.sbCount = sbCount;
    }

    public String getContainers() {
        return containers;
    }

    public void setContainers(String containers) {
        this.containers = containers;
    }

    public String getContCount() {
        return contCount;
    }

    public void setContCount(String contCount) {
        this.contCount = contCount;
    }
}
