package cbm.itp.iegmpro.Modal;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import butterknife.BindView;

public class Result implements Serializable {

    @SerializedName("UserCode")
    String userCode;

    @SerializedName("PortCode")
    String portCode;

    @SerializedName("UserId")
    String userId;

    @SerializedName("LicenseType")
    String licenseType;

    @SerializedName("CompanyId")
    String companyId;

    @SerializedName("CompanyCode")
    String companyCode;

    @SerializedName("CompanyName")
    String companyName;

    @SerializedName("LogKey")
    String logKey;

    @SerializedName("LogInTime")
    String logInTime;

    @SerializedName("ConnString")
    String connString;

    @SerializedName("Message")
    String message;

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getPortCode() {
        return portCode;
    }

    public void setPortCode(String portCode) {
        this.portCode = portCode;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLicenseType() {
        return licenseType;
    }

    public void setLicenseType(String licenseType) {
        this.licenseType = licenseType;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getLogKey() {
        return logKey;
    }

    public void setLogKey(String logKey) {
        this.logKey = logKey;
    }

    public String getLogInTime() {
        return logInTime;
    }

    public void setLogInTime(String logInTime) {
        this.logInTime = logInTime;
    }

    public String getConnString() {
        return connString;
    }

    public void setConnString(String connString) {
        this.connString = connString;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
