package cbm.itp.iegmpro.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Adi-Loch on 04/09/2018.
 */

public class PreferenceUtils {

    public static void setUserDetails(Context context, String userCode ,String portCode, String userId,
                                      String licenseType,String companyCode, String companyId, String companyName, String logKey,
                                      String logInTime, String connString, int remember) {
        SharedPreferences sharedpreferences = context.getSharedPreferences(Constant.USER_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(Constant.USERID, userId);
        editor.putString(Constant.USERCODE, userCode);
        editor.putString(Constant.PORTCODE, portCode);
        editor.putString(Constant.LICENSETYPE, licenseType);
        editor.putString(Constant.COMPANYCODE, companyCode);
        editor.putString(Constant.COMPANYID, companyId);
        editor.putString(Constant.COMPANYNAME, companyName);
        editor.putString(Constant.LOGKEY, logKey);
        editor.putString(Constant.LOGINTIME, logInTime);
        editor.putString(Constant.CONNSTRING, connString);
        editor.putInt(Constant.REMEMBER, remember);

        editor.apply();
    }

    public static String getConnString(Context context) {
        SharedPreferences pref = context.getSharedPreferences(Constant.USER_PREF, Context.MODE_PRIVATE);
        return pref.getString(Constant.CONNSTRING, "");
    }

    public static String getLogInTime(Context context) {
        SharedPreferences pref = context.getSharedPreferences(Constant.USER_PREF, Context.MODE_PRIVATE);
        return pref.getString(Constant.LOGINTIME, "");
    }

    public static String getLogKey(Context context) {
        SharedPreferences pref = context.getSharedPreferences(Constant.USER_PREF, Context.MODE_PRIVATE);
        return pref.getString(Constant.LOGKEY, "");
    }

    public static int getRemember(Context context) {
        SharedPreferences pref = context.getSharedPreferences(Constant.USER_PREF, Context.MODE_PRIVATE);
        return pref.getInt(Constant.REMEMBER, 0);
    }


    public static String getCompanyName(Context context) {
        SharedPreferences pref = context.getSharedPreferences(Constant.USER_PREF, Context.MODE_PRIVATE);
        return pref.getString(Constant.COMPANYNAME, "");
    }

    public static String getCompanyId(Context context) {
        SharedPreferences pref = context.getSharedPreferences(Constant.USER_PREF, Context.MODE_PRIVATE);
        return pref.getString(Constant.COMPANYID, "");
    }

    public static String getCompanyCode(Context context) {
        SharedPreferences pref = context.getSharedPreferences(Constant.USER_PREF, Context.MODE_PRIVATE);
        return pref.getString(Constant.COMPANYCODE, "");
    }

    public static String getLicenseType(Context context) {
        SharedPreferences pref = context.getSharedPreferences(Constant.USER_PREF, Context.MODE_PRIVATE);
        return pref.getString(Constant.LICENSETYPE, "");
    }

    public static String getPortCode(Context context) {
        SharedPreferences pref = context.getSharedPreferences(Constant.USER_PREF, Context.MODE_PRIVATE);
        return pref.getString(Constant.PORTCODE, "");
    }

    public static String getUserCode(Context context) {
        SharedPreferences pref = context.getSharedPreferences(Constant.USER_PREF, Context.MODE_PRIVATE);
        return pref.getString(Constant.USERCODE, "");
    }

    public static String getUserId(Context context) {
        SharedPreferences pref = context.getSharedPreferences(Constant.USER_PREF, Context.MODE_PRIVATE);
        return pref.getString(Constant.USERID, "");
    }

    public static void clearUserDetails(Context context) {
        SharedPreferences sharedpreferences = context.getSharedPreferences(Constant.USER_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.clear();
        editor.commit();
        editor.apply();
    }

}

