package cbm.itp.iegmpro.utils;

public class Constant {

    public static final String BASE_URL = "https://api.iegmpro.com/api/";
    public static final String DB_STRING = "YDwJbKpzi3bI/ETFNFqWGWgR2HfxVRKe3foaeHFQhO24AMe18DDxtacqM78rhjQ2hCZ8HfT2tjvk+kEDCEdoXOYhSn59aLB8AGEYZnDJvEh4Pj6Hcf8EiK9g/a7Nsfh4GVZW7paj8Kc=";

    public static final String USER_PREF = "USER_PREF";

    public static final String USERCODE = "USERCODE";
    public static final String USERID = "USERID";
    public static final String USERNAME = "USERNAME";
    public static final String PORTCODE = "PORTCODE";
    public static final String LICENSETYPE = "LICENSETYPE";
    public static final String COMPANYID = "COMPANYID";
    public static final String COMPANYNAME = "COMPANYNAME";
    public static final String LOGKEY = "LOGKEY";
    public static final String LOGINTIME = "LOGINTIME";
    public static final String CONNSTRING = "CONNSTRING";
    public static final String REMEMBER = "REMEMBER";
    public static final String COMPANYCODE = "COMPANYCODE";

    public static final String LOGIN = "GetRecords/GetMyConnLog";
    public static final String GET_EGMDATA = "LEGM/LinerDashBoard/";
    public static final String GET_EGMSBDATA = "LEGM/L_GetEGMData";
    public static final String GET_EDITEGMSB = "LEGM/L_EditEGMSB";
    public static final String GET_EDITEGMCON = "LEGM/L_EditEGMCON";

    public static int REQUEST_TAKE_PHOTO = 0;
    public static int REQUEST_IMAGE = 1;
    public static int RECORD_REQUEST_CODE = 101;
    public static int MAP_REQUEST_CODE = 101;
    public static int PLACE_PICKER_REQUEST = 102;
    public static int REQUEST_INTENT_RESULT = 103;

}
