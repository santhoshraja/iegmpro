package cbm.itp.iegmpro.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cbm.itp.iegmpro.Activities.AddSBActivity;
import cbm.itp.iegmpro.Modal.SB;
import cbm.itp.iegmpro.R;

public class SBAdapter extends RecyclerView.Adapter<SBAdapter.VesselViewHolder> implements View.OnClickListener {

    public Activity context;
    private int selectedPosition;

    public List<SB> sbs = new ArrayList<>();

    public List<SB> getSBs() {
        return sbs;
    }

    public void setSBs(List<SB> sbs) {
        this.sbs = sbs;
    }

    public int getSelectedPosition() {
        return selectedPosition;
    }

    public void setSelectedPosition(int selectedPosition) {
        this.selectedPosition = selectedPosition;
    }

    @Override
    public void onClick(View v) {
        VesselViewHolder holder = (VesselViewHolder) v.getTag();
        SB sb = this.sbs.get(holder.position);
        Log.d("id", sb.getKey() + " SB Id");
        selectedPosition = holder.position;

        switch (v.getId()){
            case R.id.layMain:
//                context.startActivity(new Intent(context, AddSBActivity.class));
                break;
        }
    }

    public static class VesselViewHolder extends RecyclerView.ViewHolder {

        ConstraintLayout layMain;
        TextView sbNo;
        TextView sbDate;
        TextView originPort;
        TextView destPort;
        TextView gateway;
        TextView gross;
        TextView noOfPkgs;
        TextView container;

        int position;

        public VesselViewHolder(View itemView) {
            super(itemView);
            this.sbDate = itemView.findViewById(R.id.txtDate);
            this.sbNo = itemView.findViewById(R.id.txtRNo);
            this.originPort = itemView.findViewById(R.id.txtOrigin);
            this.gateway = itemView.findViewById(R.id.txtGateway);
            this.destPort = itemView.findViewById(R.id.txtDest);
            this.noOfPkgs = itemView.findViewById(R.id.txtPkgs);
            this.gross = itemView.findViewById(R.id.txtGross);
            this.container = itemView.findViewById(R.id.txtDescription);

            this.layMain = itemView.findViewById(R.id.layMain);
        }
    }

    public SBAdapter(List<SB> data, Activity context) {
        this.sbs = data;
        this.context = context;
    }

    @Override
    public VesselViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.lst_sb, parent, false);
        VesselViewHolder viewHolder = new VesselViewHolder(view);

        viewHolder.layMain.setOnClickListener(this);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final VesselViewHolder holder, final int listPosition) {

        ConstraintLayout layMain = holder.layMain;
        TextView sbNo = holder.sbNo;
        TextView sbDate = holder.sbDate;
        TextView originPort = holder.originPort;
        TextView gateway = holder.gateway;
        TextView destPort = holder.destPort;
        TextView gross = holder.gross;
        TextView noOfPkgs = holder.noOfPkgs;
        TextView container = holder.container;

        SB sb = sbs.get(listPosition);

        sbNo.setText(sb.getSbNo());
        sbDate.setText(sb.getSbDate().split("T")[0]);
        gateway.setText(sb.getGatewayport());
        originPort.setText(sb.getOrginPort());
        destPort.setText(sb.getDestPort());
        noOfPkgs.setText(sb.getNoOfPkgs());
        container.setText(sb.getCargoDescr());
        gross.setText(sb.getGrossWeight());

        holder.position = listPosition;
        layMain.setTag(holder);
    }

    @Override
    public int getItemCount() {
        return sbs.size();
    }

    public void removeItem(int position) {
        sbs.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, sbs.size());
    }

}

