package cbm.itp.iegmpro.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cbm.itp.iegmpro.Activities.SBActivity;
import cbm.itp.iegmpro.Modal.CHA;
import cbm.itp.iegmpro.R;

public class RotationCHAAdapter extends RecyclerView.Adapter<RotationCHAAdapter.VesselViewHolder> implements View.OnClickListener {

    public Activity context;
    private int selectedPosition;

    public List<CHA> chas = new ArrayList<>();

    public List<CHA> getCHAs() {
        return chas;
    }

    public Activity getContext() {
        return context;
    }

    public void setCHAs(List<CHA> chas) {
        this.chas = chas;
    }

    public int getSelectedPosition() {
        return selectedPosition;
    }

    public void setSelectedPosition(int selectedPosition) {
        this.selectedPosition = selectedPosition;
    }

    @Override
    public void onClick(View v) {
        VesselViewHolder holder = (VesselViewHolder) v.getTag();
        CHA cha = this.chas.get(holder.position);
        Log.d("id", cha.getCfCode() + " CHA Id");
        selectedPosition = holder.position;

        switch (v.getId()){
            case R.id.layMain:
                context.startActivity(new Intent(context, SBActivity.class)
                .putExtra("ROTATION_CODE",cha.getRotationCode())
                .putExtra("CFCODE",cha.getCfCode())
                .putExtra("CHANAME",cha.getCfName()));
                break;
        }
    }

    public static class VesselViewHolder extends RecyclerView.ViewHolder {

        ConstraintLayout layMain;
        TextView name;
        TextView sb;
        TextView container;
        TextView sbbills;
        TextView containers;

        int position;

        public VesselViewHolder(View itemView) {
            super(itemView);
            this.name = itemView.findViewById(R.id.txtCHAName);
            this.sb = itemView.findViewById(R.id.txtSB);
            this.container = itemView.findViewById(R.id.txtCont);
            this.sbbills = itemView.findViewById(R.id.txtShippingBills);
            this.containers = itemView.findViewById(R.id.txtContainers);

            this.layMain = itemView.findViewById(R.id.layMain);
        }
    }

    public RotationCHAAdapter(List<CHA> data, Activity context) {
        this.chas = data;
        this.context = context;
    }

    @Override
    public VesselViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.lst_rotation_cha, parent, false);
        VesselViewHolder viewHolder = new VesselViewHolder(view);

        viewHolder.layMain.setOnClickListener(this);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final VesselViewHolder holder, final int listPosition) {

        ConstraintLayout layMain = holder.layMain;
        TextView name = holder.name;
        TextView sb = holder.sb;
        TextView container = holder.container;
        TextView containers = holder.containers;
        TextView sbbills = holder.sbbills;

        CHA cha = this.chas.get(listPosition);
        name.setText(cha.getCfName());
        sb.setText(cha.getSbCount());
        container.setText(cha.getContCount());
        containers.setText(cha.getContainers());
        sbbills.setText(cha.getShippingBills());

        holder.position = listPosition;
        layMain.setTag(holder);
    }

    @Override
    public int getItemCount() {
        return chas.size();
    }

    public void removeItem(int position) {
        chas.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, chas.size());
    }

}

