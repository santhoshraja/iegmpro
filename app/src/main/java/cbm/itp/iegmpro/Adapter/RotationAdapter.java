package cbm.itp.iegmpro.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cbm.itp.iegmpro.Activities.RotationActivity;
import cbm.itp.iegmpro.Modal.Rotation;
import cbm.itp.iegmpro.R;

public class RotationAdapter extends RecyclerView.Adapter<RotationAdapter.VesselViewHolder> implements View.OnClickListener {

    public Activity context;
    private int selectedPosition;

    public List<Rotation> rotations = new ArrayList<>();

    public List<Rotation> getRotations() {
        return rotations;
    }

    public void setRotations(List<Rotation> rotations) {
        this.rotations = rotations;
    }

    public int getSelectedPosition() {
        return selectedPosition;
    }

    public void setSelectedPosition(int selectedPosition) {
        this.selectedPosition = selectedPosition;
    }

    @Override
    public void onClick(View v) {
        VesselViewHolder holder = (VesselViewHolder) v.getTag();
        Rotation rotation = this.rotations.get(holder.position);
        Log.d("id", rotation.getKey() + " Rotation Code");
        selectedPosition = holder.position;

        switch (v.getId()){
            case R.id.layMain:
                context.startActivity(new Intent(context, RotationActivity.class)
                    .putExtra("ROTATION_CODE",rotation.getKey())
                    .putExtra("ROTATION",rotation.getRotationNo())
                    .putExtra("SBCOUNT",rotation.getNoOfSB())
                    .putExtra("CONTCOUNT",rotation.getNoOfContainers()));
                break;
        }
    }

    public static class VesselViewHolder extends RecyclerView.ViewHolder {

        ConstraintLayout layMain;
        TextView name;
        TextView voyage;
        TextView rotationNo;
        TextView rotationDate;
        TextView sb;
        TextView container;

        int position;

        public VesselViewHolder(View itemView) {
            super(itemView);
            this.name = itemView.findViewById(R.id.txtVesselName);
            this.voyage = itemView.findViewById(R.id.txtVoyageNo);
            this.rotationDate = itemView.findViewById(R.id.txtRDate);
            this.rotationNo = itemView.findViewById(R.id.txtRNo);
            this.sb = itemView.findViewById(R.id.txtOrigin);
            this.container = itemView.findViewById(R.id.txtPkgs);

            this.layMain = itemView.findViewById(R.id.layMain);
        }
    }

    public RotationAdapter(List<Rotation> data, Activity context) {
        this.rotations = data;
        this.context = context;
    }

    @Override
    public VesselViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.lst_rotation, parent, false);
        VesselViewHolder viewHolder = new VesselViewHolder(view);

        viewHolder.layMain.setOnClickListener(this);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final VesselViewHolder holder, final int listPosition) {

        ConstraintLayout layMain = holder.layMain;
        TextView name = holder.name;
        TextView voyage = holder.voyage;
        TextView rotationNo = holder.rotationNo;
        TextView rotationDate = holder.rotationDate;
        TextView sb = holder.sb;
        TextView container = holder.container;

        Rotation rotation = this.rotations.get(listPosition);
        name.setText(rotation.getVesselName());
        voyage.setText(rotation.getVoyageNo());
        rotationDate.setText(rotation.getRotationDate().split("T")[0]);
        rotationNo.setText(rotation.getRotationNo());
        sb.setText(rotation.getNoOfSB());
        container.setText(rotation.getNoOfContainers());

        holder.position = listPosition;
        layMain.setTag(holder);
    }

    @Override
    public int getItemCount() {
        return rotations.size();
    }

    public void removeItem(int position) {
        rotations.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, rotations.size());
    }


}

