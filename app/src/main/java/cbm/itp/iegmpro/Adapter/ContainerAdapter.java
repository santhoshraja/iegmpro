package cbm.itp.iegmpro.Adapter;

import android.app.Activity;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cbm.itp.iegmpro.Modal.Container;
import cbm.itp.iegmpro.R;

public class ContainerAdapter extends RecyclerView.Adapter<ContainerAdapter.VesselViewHolder> implements View.OnClickListener {

    public Activity context;
    private int selectedPosition;

    public List<Container> Containers = new ArrayList<>();

    public List<Container> getContainers() {
        return Containers;
    }

    public void setContainers(List<Container> Containers) {
        this.Containers = Containers;
    }

    public int getSelectedPosition() {
        return selectedPosition;
    }

    public void setSelectedPosition(int selectedPosition) {
        this.selectedPosition = selectedPosition;
    }

    @Override
    public void onClick(View v) {
        VesselViewHolder holder = (VesselViewHolder) v.getTag();
        Container Container = this.Containers.get(holder.position);
        Log.d("id", Container.getKey() + " Container Id");
        selectedPosition = holder.position;

        switch (v.getId()){
            case R.id.layMain:
//                context.startActivity(new Intent(context, AddContainerActivity.class));
                break;
        }
    }

    public static class VesselViewHolder extends RecyclerView.ViewHolder {

        ConstraintLayout layMain;
        TextView sbNo;
        TextView container;
        TextView size;

        int position;

        public VesselViewHolder(View itemView) {
            super(itemView);
            this.sbNo = itemView.findViewById(R.id.txtSBNo);
            this.size = itemView.findViewById(R.id.txtSize);
            this.container = itemView.findViewById(R.id.txtContainer);

            this.layMain = itemView.findViewById(R.id.layMain);
        }
    }

    public ContainerAdapter(List<Container> data, Activity context) {
        this.Containers = data;
        this.context = context;
    }

    @Override
    public VesselViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.lst_container, parent, false);
        VesselViewHolder viewHolder = new VesselViewHolder(view);

        viewHolder.layMain.setOnClickListener(this);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final VesselViewHolder holder, final int listPosition) {

        ConstraintLayout layMain = holder.layMain;
        TextView size = holder.size;
        TextView sbNo = holder.sbNo;
        TextView container = holder.container;

        Container Container = Containers.get(listPosition);

        sbNo.setText(Container.getSbNo());
        size.setText(Container.getContSize());
        container.setText(Container.getContainer());

        holder.position = listPosition;
        layMain.setTag(holder);
    }

    @Override
    public int getItemCount() {
        return Containers.size();
    }

    public void removeItem(int position) {
        Containers.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, Containers.size());
    }

}

